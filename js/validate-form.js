'use strict';

var formElement = document.forms['letter'];
formElement.onsubmit = validateInfoForm;

function validateInfoForm() {
    var formElement = document.forms['letter'];
    var nameElement = formElement.elements['field1'];
    var emailElement = formElement.elements['field2'];
    var textElement = formElement.elements['field3'];
    var nameValue = nameElement.value;
    var emailValue = emailElement.value;
    var textValue = textElement.value;

    if (nameValue.length > 15) {
        alert('Введите имя не длиннее 15 символов!');
        nameElement.focus();
        return false;
    }

    if (nameValue.length <= 0) {
        alert('Введите ваше имя!');
        nameElement.focus();
        return false;
    }

    if (emailValue.length <= 0) {
        alert('Введите ваш email!');
        emailElement.focus();
        return false;
    }

    if (textValue.length <= 0) {
        alert('Введите ваше сообщение!');
        textElement.focus();
        return false;
    }

    alert('Ваше письмо отравлено!');
    return true;
}