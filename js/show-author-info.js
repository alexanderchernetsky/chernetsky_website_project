'use strict';

function showInfo() {
    $('#author').fadeIn(1000);
    $('#map').fadeOut(1);
}

function hideInfo() {
    if (document.documentElement.clientWidth > 768) {
        $('#author').fadeOut(1);
        $('#map').fadeIn(1000);
    } else {
        $('#author').fadeOut(1);
    }
}