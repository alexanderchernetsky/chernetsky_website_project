'use strict';

function scrollToAboutClub() {
    var elmnt = document.getElementById('about-club');
    elmnt.scrollIntoView(false); // To bottom
}

function scrollToClubRules() {
    var elmnt = document.getElementById('club-rules');
    elmnt.scrollIntoView(false); // To bottom
}

function scrollToClubSchedule() {
    var elmnt = document.getElementById('club-schedule');
    elmnt.scrollIntoView(false); // To bottom
}

//animated scroll to coaches block
$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }
});